var gulp = require('gulp');
var loadPlugins = require('gulp-load-plugins'); //automatically requires all plugins according to package.json (install new plugins with --save-dev)
var plugins = loadPlugins();
var pathToPublicDir = '';


gulp.task('iconfont', function(){
  var fontName = 'iconsFont';
  gulp.src([pathToPublicDir + '/icons/*.svg'])
  .pipe(plugins.iconfont({
      fontName: fontName, // required 
      appendCodepoints: true, // recommended option 
      normalize: true
    }))
  .on('codepoints', function(codepoints, options) {
    gulp.src(pathToPublicDir + '/fonts/iconfont-template.css')
    .pipe(plugins.consolidate('lodash', {
      glyphs: codepoints,
      fontName: fontName,
      fontPath: '../fonts/',
      className: 'icon'
    }))
    .pipe(plugins.rename('iconfont.css'))
    .pipe(gulp.dest(pathToPublicDir + '/css/'));
    console.log(codepoints, options);

    gulp.src(pathToPublicDir + '/fonts/iconfont-html-template.html')
    .pipe(plugins.consolidate('lodash', {
      glyphs: codepoints,
      className: 'icon'
    }))
    .pipe(plugins.rename('iconfont-preview.html'))
    .pipe(gulp.dest(pathToPublicDir + '/fonts/'));
    console.log(codepoints, options);
  })
  .pipe(gulp.dest(pathToPublicDir + 'assets/fonts/'));
});


gulp.task('css', function(){
  gulp.src([pathToPublicDir + 'assets/less/all.less'])
  .pipe(plugins.less())
  .on('error', function(er){
    console.log(er.message);
    this.emit('end');
  })
  .pipe(plugins.autoprefixer({
    browsers: ['Explorer >= 8', 'Firefox > 30', 'Opera >=20', 'Android > 2', 'Chrome > 30']
  }))
  //.pipe(plugins.minifyCss())
  .pipe(plugins.concat('style.css'))
  .pipe(gulp.dest(pathToPublicDir + 'assets/css/'));
});

gulp.task('sprite', function() {
    svgSprite               = require('gulp-svg-sprite'),
    config                  = {
        mode                : {
            css             : {     // Activate the «css» mode
                render      : {
                    css     : true  // Activate CSS output (with default options)
                }
            }
        }
    };

    gulp.src(pathToPublicDir + 'assets/img/svg/**/*.svg')
        .pipe(svgSprite(config))
            .on('error', function(er){
                console.log(er.message);
            })
        .pipe(gulp.dest(pathToPublicDir + 'assets/img/sprites'));
});

gulp.task('js', function(){
  gulp.src(pathToPublicDir + 'assets/js/*.js')
        //.pipe(plugins.uglify())
        .pipe(plugins.concat('app.min.js'))
        .pipe(gulp.dest(pathToPublicDir + 'assets/js'));
      });

gulp.task('watch', function(){
  gulp.watch(pathToPublicDir + 'assets/less/**/*.less', ['css']);
  gulp.watch(pathToPublicDir + 'assets/js/*.js', ['js']);
  //gulp.watch(pathToPublicDir + '/icons/*.svg', ['iconfont']);

  plugins.livereload.listen();
  gulp.watch([
    pathToPublicDir + 'index.html',
    pathToPublicDir + 'assets/css/*.css',
    pathToPublicDir + 'assets/js/*.js',
    '**/*.latte'
    ]).on("change", plugins.livereload.changed);
});

gulp.task('default', ['css', 'js'/*, 'iconfont'*/]);
//gulp.task('default', []);